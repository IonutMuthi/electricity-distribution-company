package model;

import java.sql.Date;

public class Bill {

private int id;	
private double price;
private boolean paid;
private Date emited;
private Date scadent;
//private int clientNumber;

public Bill(int id,double price,Date emited, Date scadent,boolean paid){
	this.setId(id);
	this.price=price;
	this.setEmited(emited);
	this.setScadent(scadent);
	this.paid=paid;
}


public double getPrice() {
	return price;
}
public void setPrice(double price) {
	this.price = price;
}
public boolean isPaid() {
	return paid;
}
public void setPaid(boolean paid) {
	this.paid = paid;
	
}



public double rest(double sum){
	return sum;
}

public int getId() {
	return id;
}

public void setId(int id) {
	this.id = id;
}

public Date getEmited() {
	return emited;
}

public void setEmited(Date emited) {
	this.emited = emited;
}

public Date getScadent() {
	return scadent;
}

public void setScadent(Date scadent) {
	this.scadent = scadent;
}


	
	
}
