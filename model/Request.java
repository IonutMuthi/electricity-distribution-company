package model;

public class Request {
	private String req;
	private boolean accept;
	private int clientNumber;
	private int idRequest;
	private int idComplaint;
	
	public Request(int idRequest,String req, boolean accept,int clientNumber,int idComplaint){
		this.req=req;
		this.idRequest=idRequest;
		this.accept=accept;
		this.idComplaint=idComplaint;
		this.clientNumber=clientNumber;
	}
	
	public String getReq() {
		return req;
	}
	public void setReq(String req) {
		this.req = req;
	}
	public boolean isAccept() {
		return accept;
	}
	public void setAccept(boolean accept) {
		this.accept = accept;
	}

	public int getClientNumber() {
		return clientNumber;
	}

	public void setClientNumber(int clientNumber) {
		this.clientNumber = clientNumber;
	}

	public int getIdRequest() {
		return idRequest;
	}

	public int getIdComplaint() {
		return idComplaint;
	}

	public void setIdComplaint(int idComplaint) {
		this.idComplaint = idComplaint;
	}


	

}
