package model;

public  class User {
	private String username;
	private String password;
	private int idCardNumber;
	private String name;
	private String address;
	
	
	public String getUserame() {
		return username;
	}
	public void setUserame(String username) {
		this.username = username;
	}
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getIdCardNumber() {
		return idCardNumber;
	}
	public void setIdCardNumber(int idCardNumber) {
		this.idCardNumber = idCardNumber;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	

}
