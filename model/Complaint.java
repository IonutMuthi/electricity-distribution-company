package model;

import java.sql.Date;

public class Complaint {
	private String description;
	private Date date;
	private int  serviceQuality;
	private int quickResponse;
	private int costumerServiceQuality;
	private int clientNumber;
	private int idComplaint;
	
	
	public Complaint(String description, Date date, int serviceQuality, int quickResponse, int costumerServiceQuality,int clientNumber, int idComplaint){
		this.description=description;
		this.date=date;
		this.serviceQuality=serviceQuality;
		this.quickResponse=quickResponse;
		this.costumerServiceQuality= costumerServiceQuality;
		this.idComplaint=idComplaint;
		this.setClientNumber(clientNumber);
	}
	
	
	// posibil sterg settere si las doar gettere 
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public int getServiceQuality() {
		return serviceQuality;
	}
	public void setServiceQuality(int serviceQuality) {
		this.serviceQuality = serviceQuality;
	}
	public int getQuickResponse() {
		return quickResponse;
	}
	public void setQuickResponse(int quickResponse) {
		this.quickResponse = quickResponse;
	}
	public int getCostumerServiceQuality() {
		return costumerServiceQuality;
	}
	public void setCostumerServiceQuality(int costumerServiceQuality) {
		this.costumerServiceQuality = costumerServiceQuality;
	}


	public int getClientNumber() {
		return clientNumber;
	}


	public void setClientNumber(int clientNumber) {
		this.clientNumber = clientNumber;
	}


	public int getIdComplaint() {
		return idComplaint;
	}


	public void setIdComplaint(int idComplaint) {
		this.idComplaint = idComplaint;
	}
	
	

}
