package UI;


import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import bll.ClientBLL;
import bll.EmployeeBLL;
import bll.ManagerBLL;

import model.Client;
import model.Employee;
import model.Manager;

public class LogIn extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField txtUsername;
	private JTextField txtPassword;
	private JLabel lblPassword;
	private JLabel lblUserName;
	private ClientUI c;
	private Client cl;
	private Employee em;
	private EmployeeUI eui;
	private Manager m;
	private ManagerUI mui;
	private String user,password;
	private int cnt=0;
	private JLabel lblClientNumber;
	private JTextField textField;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LogIn frame = new LogIn();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public LogIn() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 300, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		
		JLabel lblUsernameOrPassword = new JLabel("");
		lblUsernameOrPassword.setVisible(false);
		lblUsernameOrPassword.setForeground(Color.RED);
		lblUsernameOrPassword.setBounds(77, -12, 207, 48);
		contentPane.add(lblUsernameOrPassword);
		
		JButton btnAutentification = new JButton("Autentification");
		btnAutentification.addMouseListener(new MouseAdapter() {
			
			@Override
			public void mouseClicked(MouseEvent e) {
				
	          try {
	        	 user=txtUsername.getText();
	        	 password=txtPassword.getText();
	        	 System.out.println(user);
	        	  cl=ClientBLL.findByUsername(user);
	        	  em=EmployeeBLL.findByUsername(user);
	        	  m=ManagerBLL.findManagerByUsername(user);
	        	  if(cl==null){
	        		 
	        		  cnt++;
	        		  
	        	  }else{
	        		  if(cl.getPassword().equals(password)){
		        		  System.out.println("Client "+cl.getClientNumber() +" log");
						c=new ClientUI(cl.getClientNumber());				
						c.setVisible(true);
						
		
		        	  }else{
		        		  lblUsernameOrPassword.setText("Incorrect password");
		        		  lblUsernameOrPassword.setVisible(true);
					}
		        		  
	        		  
	        	  }
	        	 if(em==null){
	        		 
	        		cnt++;
	        	 }else{
	     	        		    	        	 
	   	        	  if(em.getPassword().equals(password)){
	   	        		  System.out.println("employee " +" log");
	   	        		  eui=new EmployeeUI();
	   					  eui.setVisible(true);
	   				
	   	        	  }else{
	   	        		  lblUsernameOrPassword.setText("Incorrect password employee");
	   	        		  lblUsernameOrPassword.setVisible(true);
	   				}
	   	        	  
	        		 }
	        	 
	          if(m==null){
	        	  				cnt++;
	        			  }else{
	          
        	  if(m.getPassword().equals(password)){
        		  System.out.println("manager " +" log");
				mui=new ManagerUI();				
				mui.setVisible(true);
				
		
        	  }else{
        		  lblUsernameOrPassword.setText("Incorrect password manager");
        		  lblUsernameOrPassword.setVisible(true);
			}
	        			  }
        	  if(cnt==3){
        		  
        		  lblUsernameOrPassword.setText("User dosen't exist");
        		  lblUsernameOrPassword.setVisible(true);
        	  }
	        	  
	        	  
	          }catch (Exception e1) {
					e1.printStackTrace();
					
				}
	          
			}
			
		});
		
		lblUserName = new JLabel("User name:");
		lblUserName.setBounds(5, 40, 80, 14);
		contentPane.add(lblUserName);
		
		btnAutentification.setBounds(40, 120, 200, 23);
		contentPane.add(btnAutentification);
		
		

		
		txtUsername = new JTextField();
		txtUsername.setBounds(110, 30, 110, 30);
		contentPane.add(txtUsername);
		txtUsername.setColumns(10);
		
		txtPassword = new JTextField();
		txtPassword.setFont(new Font("Symbol", Font.PLAIN, 11));
		txtPassword.setBackground(new Color(255, 255, 255));
		txtPassword.setBounds(110, 70, 110, 30);
		contentPane.add(txtPassword);
		txtPassword.setColumns(10);
		
		lblPassword = new JLabel("Password:");
		lblPassword.setBounds(5, 80, 80, 14);
		contentPane.add(lblPassword);
		
		lblClientNumber = new JLabel("Client Number: ");
		lblClientNumber.setBounds(5, 190, 120, 14);
		contentPane.add(lblClientNumber);
		
		textField = new JTextField();
		textField.setBounds(110, 180, 110, 30);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JButton btnRequestNewAccount = new JButton("Request new account ");
		btnRequestNewAccount.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int cnr=Integer.parseInt(textField.getText());
				try {
				
				Client c= ClientBLL.findById(cnr);
				
				if(c==null){
					  lblUsernameOrPassword.setText("Client number dosen't exist");
	        		  lblUsernameOrPassword.setVisible(true);
				}else{
					
					AccountRequest r= new AccountRequest(cnr);
					r.setVisible(true);
				}
				
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
		});
		btnRequestNewAccount.setBounds(40, 220, 200, 23);
		contentPane.add(btnRequestNewAccount);
		
		
		
	}
}
