package UI;


import java.awt.EventQueue;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;

import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import connection.ConnectionFactory;
import dao.BillDAO;
import bll.BillBLL;

import model.Bill;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class ShowBillUI extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTable table;
	ConnectionFactory conFact= new ConnectionFactory();
	private JTextField textBillId;
	private JButton btnNewButton;
	private JLabel lblBillId;
	private JTextField textSum;
	private static int clientNumber;


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					
					ShowBillUI frame = new ShowBillUI(clientNumber);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
		});
		
	}

	public int getClientNumber() {
		return clientNumber;
	}


	
	/**
	 * Create the frame.
	 * @param clientNumber 
	 * @throws SQLException 
	 */
	
	public DefaultTableModel updateTable(int idClient) throws SQLException{
		
		
		String[] cols={"Bill id","Emited date","Scadent date","Price"};
		
		DefaultTableModel model = new DefaultTableModel();
		model.addColumn("Bill id");
		model.addColumn("Emited date");
		model.addColumn("Scadent date");
		model.addColumn("Price4");
		model.addRow(cols);
			for(Bill b:BillBLL.getBills(idClient)){
				if(!(b.isPaid())){
					Object[] o={b.getId(),b.getEmited(),b.getScadent(),b.getPrice()};
					model.addRow(o);
					System.out.println(idClient+" "+b.getId()+" "+b.getPrice()+" " +b.getEmited()+" "+b.getScadent()+" "+b.isPaid());
				}
			
			}
			
	return model;

	
	}
	
	public ShowBillUI(int id) throws SQLException {
		setTitle("Unpaid bills");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 340);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		

	

		table = new JTable(updateTable(id));
		table.setBounds(10, 11, 414, 189);
		contentPane.add(table);
		
		textBillId = new JTextField();
		textBillId.setBounds(150, 240, 86, 20);
		contentPane.add(textBillId);
		textBillId.setColumns(10);
		
		
		
		lblBillId = new JLabel("Bill id:");
		lblBillId.setBounds(150, 214, 86, 20);
		contentPane.add(lblBillId);
		
		textSum = new JTextField();
		textSum.setBounds(260, 240, 86, 20);
		contentPane.add(textSum);
		textSum.setColumns(10);
		
		JLabel lblAmount = new JLabel("Amount:");
		lblAmount.setBounds(260, 211, 86, 20);
		contentPane.add(lblAmount);
		
		
		JLabel lblRaspuns = new JLabel("");
		lblRaspuns.setVisible(false);
		lblRaspuns.setBounds(90, 270, 250, 30);
		contentPane.add(lblRaspuns);
		
		btnNewButton = new JButton("Pay");
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				try {
					Bill b=BillDAO.findById(Integer.parseInt(textBillId.getText()), id);
					double sum=Double.parseDouble(textSum.getText());
				    double price=b.getPrice();
					
					if(price>sum){
						lblRaspuns.setText("Not enough money");
						lblRaspuns.setVisible(true);
						
					}else if(price<=sum){
						double rest= sum-price;
						lblRaspuns.setText("Paiment done your rest is: " + rest + "$");
						lblRaspuns.setVisible(true);
						b.setPaid(true);
						BillBLL.updateBill(b, id);
						table.setModel(updateTable(id));
						
						
					}
					
					
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				repaint();
				validate();
				
			}
		});
		btnNewButton.setBounds(30, 240, 89, 23);
		contentPane.add(btnNewButton);
		

	}
}
