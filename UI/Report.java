package UI;


import java.awt.EventQueue;
import java.sql.SQLException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import dao.ComplaintDAO;
import model.Complaint;

import javax.swing.JLabel;

public class Report extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
    private double avgSQ=0.0;
    private double avgQR=0.0;
    private double avgCSQ=0.0;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Report frame = new Report();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @throws SQLException 
	 */
	public Report() throws SQLException {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 310, 150);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblQr = new JLabel("");
		lblQr.setBounds(10, 10, 300, 20);
		contentPane.add(lblQr);
		
		JLabel lblSq = new JLabel("");
		lblSq.setBounds(10, 40, 300, 20);
		contentPane.add(lblSq);
		
		JLabel lblCsq = new JLabel("");
		lblCsq.setBounds(10, 70, 300, 20);
		contentPane.add(lblCsq);
		
		int sq=0,qr=0,csq=0;
		
		for(Complaint c:bll.ComplaintBLL.getComplaints()){
			
			sq+=c.getServiceQuality();
			qr+=c.getQuickResponse();
			csq+=c.getCostumerServiceQuality();
			
		}
		
		int cNr=bll.ComplaintBLL.getComplaints().size();
		
		avgSQ=(double)sq/cNr;
		avgQR=(double)qr/cNr;
		avgCSQ=(double)csq/cNr;
		
		lblQr.setText("Quick response average is : "+ avgQR);
		lblSq.setText("Service quality average is : "+ avgSQ);
		lblCsq.setText("Costumer servide quality avreage is :"+avgCSQ);
		
		
	}
}
