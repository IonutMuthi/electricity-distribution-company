package UI;


import java.awt.EventQueue;
import java.sql.SQLException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import connection.ConnectionFactory;

import dao.RequestDAO;

import model.Request;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class ComplaintRequestUI extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTable table;
	ConnectionFactory conFact= new ConnectionFactory();


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ComplaintRequestUI frame = new ComplaintRequestUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

public DefaultTableModel updateTable() throws SQLException{
		
		
		String[] cols={"Request number","Description","ClientNumber"};
		
		DefaultTableModel model = new DefaultTableModel();
		model.addColumn("Request number");
		model.addColumn("Description");
		model.addColumn("ClientNumber");
		
		model.addRow(cols);
			for(Request r:RequestDAO.getRequests()){
				
				if(!r.isAccept()){
		    	Object[] o={r.getIdRequest(),r.getReq(),r.getClientNumber()};
				model.addRow(o);
				}
			}
			
	return model;

	
	}
	
	/**
	 * Create the frame.
	 * @throws SQLException 
	 */
	public ComplaintRequestUI() throws SQLException {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		
		table = new JTable(updateTable());
		table.setBounds(5, 5, 424, 200);
		contentPane.add(table);
		
		
		// vezi detalii
		JButton btnAcceptRequest = new JButton("Accept request");
		btnAcceptRequest.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
								
				try {
				int id=Integer.parseInt(updateTable().getValueAt(table.getSelectedRow(), 0).toString());
				//we take the value from the selected row at column 0 and update the corespondent request 
				RequestDAO.updateRequest(id);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnAcceptRequest.setBounds(30, 220, 150, 25);
		contentPane.add(btnAcceptRequest);
		
	}

}
