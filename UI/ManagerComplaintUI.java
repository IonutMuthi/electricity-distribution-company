package UI;

import java.awt.EventQueue;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Date;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import connection.ConnectionFactory;
import model.Complaint;

public class ManagerComplaintUI extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTable table;
	ConnectionFactory conFact= new ConnectionFactory();
	Complaint c ;
  

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ManagerComplaintUI frame = new ManagerComplaintUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	
@SuppressWarnings("deprecation")
public DefaultTableModel updateTable() throws SQLException{
		
		
		String[] cols={"Description","Date","Client number","Service quality","Quick response","Costumer service quality","Complaint number"};
		
		DefaultTableModel model = new DefaultTableModel();
		model.addColumn("Description");
		model.addColumn("Client number");
		model.addColumn("Date");
		model.addColumn("Service quality");
		model.addColumn("Quick response");
		model.addColumn("Costumer service quality");
		model.addColumn("Complaint number");
		model.addRow(cols);
			for(Complaint c : bll.ComplaintBLL.getComplaints()){
				 
				 
				Date d=c.getDate();
		     	int dd = d.getDate();
				int mm = d.getMonth()+1; //January is 0!
				int yyyy = d.getYear()+1900; // time is only mesured form 1900 to curent date so we need to add 1900 to ghet the correct year
				
				String date1=""+dd+"/"+mm+"/"+yyyy;
		    	
		    	
				Object[] o={c.getDescription(),date1,c.getClientNumber(),c.getCostumerServiceQuality(),
		    			c.getQuickResponse(),c.getCostumerServiceQuality(),c.getIdComplaint()};
				model.addRow(o);
					
			}
			
			String[] empty={"","","","","","",""}; // we make sure last row is empty so we can add new clients
			model.addRow(empty);
			
	return model;

	
	}
	
	/**
	 * Create the frame.
	 * @throws SQLException 
	 */
	public ManagerComplaintUI() throws SQLException {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 900, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
	
		
		
		table = new JTable(updateTable());
		
	
		table.setBounds(5, 0, 869, 200);
		contentPane.add(table);
		
		 table.getModel().addTableModelListener(new TableModelListener() {
				
				@Override
				public void tableChanged(TableModelEvent e) {
					int sq=0,qr=0,csq=0,cn=0,idc=0;
					
					String description=(String) table.getModel().getValueAt(table.getSelectedRow(),0);
				
	
					if(table.getModel().getValueAt(table.getSelectedRow(),2) instanceof String ){
						String sqs=(String) table.getModel().getValueAt(table.getSelectedRow(),2);
						if(!sqs.equals("")){
						sq=Integer.parseInt(sqs);
						}
					  
					}else{
						 sq=(int) table.getModel().getValueAt(table.getSelectedRow(),2);
					}
					
					if(table.getModel().getValueAt(table.getSelectedRow(),3) instanceof String ){
						String qrs=(String) table.getModel().getValueAt(table.getSelectedRow(),3);
						if(!qrs.equals("")){
						qr=Integer.parseInt(qrs);
						}
					  
					}else{
						 qr=(int) table.getModel().getValueAt(table.getSelectedRow(),3);
					}
					
					if(table.getModel().getValueAt(table.getSelectedRow(),4) instanceof String ){
						String csqs=(String) table.getModel().getValueAt(table.getSelectedRow(),4);
						if(!csqs.equals("")){
						csq=Integer.parseInt(csqs);
						}
					  
					}else{
						 csq=(int) table.getModel().getValueAt(table.getSelectedRow(),4);
					}
					
					
					if(table.getModel().getValueAt(table.getSelectedRow(),5) instanceof String){
						String cns=(String) table.getModel().getValueAt(table.getSelectedRow(),5);
						if(!cns.equals("")){
						cn=Integer.parseInt(cns);
				       }
						 
							
					}else{
						cn=(int) table.getModel().getValueAt(table.getSelectedRow(),5);
					}
						
					
					if(table.getModel().getValueAt(table.getSelectedRow(),6) instanceof String ){
						String idcs=(String) table.getModel().getValueAt(table.getSelectedRow(),6);
						if(!idcs.equals("")){
						idc=Integer.parseInt(idcs);
						}
					  
					}else{
						 idc=(int) table.getModel().getValueAt(table.getSelectedRow(),6);
					}
					
					
				
					String dateS =(String) table.getModel().getValueAt(table.getSelectedRow(), 1);
					
					Date date = null ;
							
					if(!dateS.equals("")){
						java.util.Date javaDate = null;
						
						try {
							DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
							javaDate = df.parse(dateS);
						} catch (ParseException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						date = new java.sql.Date(javaDate.getTime());
					}
					
				
					
					c =new Complaint(description,date,sq,qr,csq,cn,idc);
					System.out.println(c.getIdComplaint()+" "+c.getDescription()+" "+c.getDate()+" "+c.getServiceQuality()+" "+c.getQuickResponse()+" "+c.getCostumerServiceQuality()+" "+c.getClientNumber());		
					
				}
				});
		 
		 

		
		JButton btnNewButton = new JButton("Update ");
		
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
		          try {
		        	 
		        
		        	  bll.ComplaintBLL.updateComplaint(c);
					table.setModel(updateTable());
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		      	repaint();
				validate();
			}
		});
		btnNewButton.setBounds(30, 210, 89, 23);
		contentPane.add(btnNewButton);
		
		
		
		
		
			
		
		JButton btnAdd = new JButton("Add");
		btnAdd.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
						
					try {
			
						bll.ComplaintBLL.addComplaint(c);
					table.setModel(updateTable());
					
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
					repaint();
					validate();
			}
		});
		btnAdd.setBounds(130, 210, 89, 23);
		contentPane.add(btnAdd);
		
		JButton btnDelete = new JButton("Delete");
		btnDelete.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				try {
					int cn=(int)table.getModel().getValueAt(table.getSelectedRow(), 6);
					bll.ComplaintBLL.deleteComplaint(cn);
					table.setModel(updateTable());
					
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				repaint();
				validate();
			}
		});
		btnDelete.setBounds(230, 210, 89, 23);
		contentPane.add(btnDelete);
		
	}

}
