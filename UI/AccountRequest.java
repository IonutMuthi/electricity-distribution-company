package UI;


import java.awt.EventQueue;
import java.awt.GraphicsConfiguration;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import bll.ClientBLL;
import dao.ClientDAO;
import model.Client;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.SQLException;

public class AccountRequest extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private static int clientNumber;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					
					AccountRequest frame = new AccountRequest(clientNumber);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @param cnr 
	 */
	public AccountRequest(int cnr) {
		setTitle("New Account Request");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 290, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblName = new JLabel("Name: ");
		lblName.setBounds(10, 20, 46, 14);
		contentPane.add(lblName);
		
		textField = new JTextField();
		textField.setBounds(120, 20, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JLabel lblUsername = new JLabel("Username:");
		lblUsername.setBounds(10, 50, 70, 14);
		contentPane.add(lblUsername);
		
		textField_1 = new JTextField();
		textField_1.setBounds(120, 50, 86, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Password:");
		lblNewLabel.setBounds(10, 80, 70, 14);
		contentPane.add(lblNewLabel);
		
		textField_2 = new JTextField();
		textField_2.setBounds(120, 80, 86, 20);
		contentPane.add(textField_2);
		textField_2.setColumns(10);
		
		JLabel lblRepetPassword = new JLabel("Repet password:");
		lblRepetPassword.setBounds(10, 110, 98, 14);
		contentPane.add(lblRepetPassword);
		
		textField_3 = new JTextField();
		textField_3.setBounds(120, 110, 86, 20);
		contentPane.add(textField_3);
		textField_3.setColumns(10);
		
		JLabel lblAddress = new JLabel("Address: ");
		lblAddress.setBounds(10, 140, 70, 14);
		contentPane.add(lblAddress);
		
		textField_4 = new JTextField();
		textField_4.setText("");
		textField_4.setBounds(120, 140, 86, 20);
		contentPane.add(textField_4);
		textField_4.setColumns(10);
		
		JLabel lblIdCardNumber = new JLabel("Id card number:");
		lblIdCardNumber.setBounds(10, 170, 98, 14);
		contentPane.add(lblIdCardNumber);
		
		textField_5 = new JTextField();
		textField_5.setBounds(120, 170, 86, 20);
		contentPane.add(textField_5);
		textField_5.setColumns(10);
		
	
		
		JButton btnRequest = new JButton("Request");
		btnRequest.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Client c;
				String name=textField.getText();
				String username=textField_1.getText();
				String password=textField_2.getText();
				String rpassword=textField_3.getText();
				String adr=textField_4.getText();
				int idCardNumber=Integer.parseInt(textField_5.getText());
				
				if(password.equals(rpassword)){
					c=new Client(cnr,username,password,name,adr,idCardNumber);
					try {
						ClientBLL.updateClient(c, cnr);
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
				
			}
		});
		btnRequest.setBounds(80, 230, 89, 23);
		contentPane.add(btnRequest);
	}

}
