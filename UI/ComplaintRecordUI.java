package UI;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.sql.Date;
import java.sql.SQLException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import connection.ConnectionFactory;

import dao.RequestDAO;
import model.Complaint;
import model.Request;

public class ComplaintRecordUI extends JFrame {

	private JPanel contentPane;
	private JTable table;
	ConnectionFactory conFact= new ConnectionFactory();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ComplaintRecordUI frame = new ComplaintRecordUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
public DefaultTableModel updateTable() throws SQLException{
		
		
		String[] cols={"Client number","Date","Service quality","Quick response","Costumer service quality"};
		
		DefaultTableModel model = new DefaultTableModel();
		model.addColumn("Client number");
		model.addColumn("Date");
		model.addColumn("Service quality");
		model.addColumn("Quick response");
		model.addColumn("Costumer service quality");
		model.addRow(cols);
			for(Complaint c : bll.ComplaintBLL.getComplaints()){
				 
				Date d=c.getDate();
		     	int dd = d.getDate();
				int mm = d.getMonth()+1; //January is 0!
				int yyyy = d.getYear(); // time is only mesured form 1900 to curent date so we need to add 1900 to ghet the correct year
				
				String date1=""+dd+"/"+mm+"/"+yyyy;
		    	
				Object[] o={c.getClientNumber(),date1,c.getCostumerServiceQuality(),
		    			c.getQuickResponse(),c.getCostumerServiceQuality()};
				model.addRow(o);
					
			}
			
	return model;

	
	}

	/**
	 * Create the frame.
	 * @throws SQLException 
	 */
	public ComplaintRecordUI() throws SQLException {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 750, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		
		table = new JTable(updateTable());
		table.setBounds(5, 5, 729, 200);
		contentPane.add(table);
		
	}

}
