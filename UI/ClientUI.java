package UI;

//import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;


import dao.RequestDAO;
import model.Complaint;

import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.SQLException;

public class ClientUI extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textRequest;
	private static int clientNumber;
    

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ClientUI frame = new ClientUI(clientNumber);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @param i 
	 * @throws SQLException 
	 */
	public ClientUI(int id) throws SQLException {
		setTitle("Electric App Client");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnSeeUnpayedBills = new JButton("See unpayed bills");
		btnSeeUnpayedBills.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				ShowBillUI b;
				try {
				
					b = new ShowBillUI(id);		
				    b.setVisible(true); 
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		});
		btnSeeUnpayedBills.setBounds(10, 30, 150, 23);
		contentPane.add(btnSeeUnpayedBills);
		
		JButton btnIstoricOfBills = new JButton("Istoric of bills");
		btnIstoricOfBills.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				IstoricUI i;
				try {
					 i= new IstoricUI(id);
					 i.setVisible(true);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		});
		btnIstoricOfBills.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnIstoricOfBills.setBounds(10, 60, 150, 23);
		contentPane.add(btnIstoricOfBills);
		
		JButton btnRequestComplaint = new JButton("Request complaint");
		btnRequestComplaint.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				try {
					RequestDAO.addRequest(textRequest.getText(),id);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		btnRequestComplaint.setBounds(10, 90, 150, 23);
		contentPane.add(btnRequestComplaint);
		
		textRequest = new JTextField();
		textRequest.setText("Shortly describe the problem");
		textRequest.setBounds(180, 90, 192, 20);
		contentPane.add(textRequest);
		textRequest.setColumns(10);
		
		JButton btnNewAccount = new JButton("New account ");
		btnNewAccount.setBounds(10, 120, 150, 23);
		contentPane.add(btnNewAccount);
		
		JButton btnFileComplaint = new JButton("File complaint");
		btnFileComplaint.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				ComplaintUI cui;
				try {
					cui = new ComplaintUI(id);
					cui.setVisible(true);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
		});
		btnFileComplaint.setBounds(10, 170, 150, 23);
		btnFileComplaint.setEnabled(enabled(id));
		contentPane.add(btnFileComplaint);
	}
	
	private boolean enabled(int id) throws SQLException{
		
		for(Complaint c :bll.ComplaintBLL.getComplaints()){
			System.out.println(c.getDescription());
			if(!c.getDescription().isEmpty()){
				if(c.getClientNumber()==id){
				return true;
				}
			}
		}
	
		
		return false;
	}

	public int getClientNumber() {
		return clientNumber;
	}

	public void setClientNumber(int clientNumber) {
		this.clientNumber = clientNumber;
	}

	
}
