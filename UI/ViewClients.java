package UI;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.sql.SQLException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;

import connection.ConnectionFactory;
import dao.BillDAO;

import model.Bill;
import model.Client;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class ViewClients extends JFrame {

	private JPanel contentPane;
	private JTable table;
	ConnectionFactory conFact= new ConnectionFactory();
    private Client c;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ViewClients frame = new ViewClients();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	
public DefaultTableModel updateTable() throws SQLException{
		
		
		String[] cols={"Client number","Username","Password","Name","Address","idCardNumber"};
		
		DefaultTableModel model = new DefaultTableModel();
		model.addColumn("Client number");
		model.addColumn("Username");
		model.addColumn("Password");
		model.addColumn("Name");
		model.addColumn("Address");
		model.addColumn("idCardNumber");
		model.addRow(cols);
			for(Client c:bll.ClientBLL.getClients()){
				
				
				Object[] o={c.getClientNumber(),c.getUserame(),c.getPassword(),c.getName(),c.getAddress(),c.getIdCardNumber()};
				model.addRow(o);
			
			
			}
			
	return model;

	
	}

	/**
	 * Create the frame.
	 * @throws SQLException 
	 */
	public ViewClients() throws SQLException {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		table = new JTable(updateTable());
		table.setBounds(5, 0, 424, 200);
		contentPane.add(table);
		
		
		table.getModel().addTableModelListener(new TableModelListener() {
			
			@Override
			public void tableChanged(TableModelEvent arg0) {
				int cN=(int) table.getModel().getValueAt(table.getSelectedRow(),0);
				String uName=(String) table.getModel().getValueAt(table.getSelectedRow(),1);
				String pass=(String) table.getModel().getValueAt(table.getSelectedRow(),2);
				String name=(String) table.getModel().getValueAt(table.getSelectedRow(),3);
				String adr=(String) table.getModel().getValueAt(table.getSelectedRow(),4);
				int card=(int) table.getModel().getValueAt(table.getSelectedRow(),5);
				
				 c =new Client(cN,uName,pass,name,adr,card);
				System.out.println(c.getClientNumber()+" " +c.getUserame()+" "+c.getPassword()+" "+c.getName()+" "+c.getAddress()+" "+c.getIdCardNumber());		
				
			}
			});
		
		JButton btnNewButton = new JButton("Update ");
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
		          try {
		        	
					bll.ClientBLL.updateClient(c, c.getClientNumber());
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		});
		btnNewButton.setBounds(30, 210, 89, 23);
		contentPane.add(btnNewButton);
		
	}

}
