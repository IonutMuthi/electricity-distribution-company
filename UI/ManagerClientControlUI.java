package UI;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;

import bll.ClientBLL;
import connection.ConnectionFactory;
import dao.ClientDAO;
import model.Client;


public class ManagerClientControlUI extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTable table;
	ConnectionFactory conFact= new ConnectionFactory();
	Client c ;
  
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ManagerClientControlUI frame = new ManagerClientControlUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	
	
public DefaultTableModel updateTable() throws SQLException{
		
		
		String[] cols={"Client number","Username","Password","Name","Address","idCardNumber"};
		
		DefaultTableModel model = new DefaultTableModel();
		model.addColumn("Client number");
		model.addColumn("Username");
		model.addColumn("Password");
		model.addColumn("Name");
		model.addColumn("Address");
		model.addColumn("idCardNumber");
		model.addRow(cols);
			for(Client c:ClientBLL.getClients()){
				
				
				Object[] o={c.getClientNumber(),c.getUserame(),c.getPassword(),c.getName(),c.getAddress(),c.getIdCardNumber()};
				model.addRow(o);
			
			
			}
			String[] empty={"","","","","",""}; // we make sure last row is empty so we can add new clients
			model.addRow(empty);
			
	return model;

	
	}

	/**
	 * Create the frame.
	 * @throws SQLException 
	 */
	public ManagerClientControlUI() throws SQLException {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		table = new JTable(updateTable());
		
	
		table.setBounds(5, 0, 424, 200);
		contentPane.add(table);
		
		 table.getModel().addTableModelListener(new TableModelListener() {
				
				@Override
				public void tableChanged(TableModelEvent e) {
					int cN,card=0;
					if(table.getModel().getValueAt(table.getSelectedRow(),0) instanceof String ){
						String cNr=(String) table.getModel().getValueAt(table.getSelectedRow(),0);
						 cN=Integer.parseInt(cNr);
						
					  
					}else{
						 cN=(int) table.getModel().getValueAt(table.getSelectedRow(),0);
					}
					
					if(table.getModel().getValueAt(table.getSelectedRow(),5) instanceof String){
						String cardS=(String) table.getModel().getValueAt(table.getSelectedRow(),5);
						if(!cardS.equals("")){
						card=Integer.parseInt(cardS);
				       }
						 
							
					}else{
						card=(int) table.getModel().getValueAt(table.getSelectedRow(),5);
					}
						
					String uName=(String) table.getModel().getValueAt(table.getSelectedRow(),1);
					String pass=(String) table.getModel().getValueAt(table.getSelectedRow(),2);
					String name=(String) table.getModel().getValueAt(table.getSelectedRow(),3);
					String adr=(String) table.getModel().getValueAt(table.getSelectedRow(),4);
							
			
					 c =new Client(cN,uName,pass,name,adr,card);
					System.out.println(c.getClientNumber()+" " +c.getUserame()+" "+c.getPassword()+" "+c.getName()+" "+c.getAddress()+" "+c.getIdCardNumber());		
					
				}
				});
		 
		 

		
		JButton btnNewButton = new JButton("Update ");
		
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
		          try {
		        	 
		        
					ClientBLL.updateClient(c, c.getClientNumber());
					table.setModel(updateTable());
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		      	repaint();
				validate();
			}
		});
		btnNewButton.setBounds(30, 210, 89, 23);
		contentPane.add(btnNewButton);
		
		
		
		
		
			
		
		JButton btnAdd = new JButton("Add");
		btnAdd.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
						
					try {
			
					System.out.println(c.getClientNumber());
					ClientBLL.insertClient(c);
					//ClientDAO.addClient(c);
					table.setModel(updateTable());
					
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
					repaint();
					validate();
			}
		});
		btnAdd.setBounds(130, 210, 89, 23);
		contentPane.add(btnAdd);
		
		JButton btnDelete = new JButton("Delete");
		btnDelete.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				try {
					int cn=(int)table.getModel().getValueAt(table.getSelectedRow(), 0);
					ClientBLL.deleteClient(cn);
					table.setModel(updateTable());
					
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				repaint();
				validate();
			}
		});
		btnDelete.setBounds(230, 210, 89, 23);
		contentPane.add(btnDelete);
		
	}
	

	
	
}


