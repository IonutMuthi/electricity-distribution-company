package UI;


import java.awt.EventQueue;


import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;


import model.Complaint;


import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JComboBox;

import javax.swing.JScrollPane;
import java.awt.Label;
import java.sql.Date;
import java.sql.SQLException;

import javax.swing.DefaultComboBoxModel;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class ComplaintUI extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private static int clientNumber;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					
					ComplaintUI frame = new ComplaintUI(clientNumber);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @param clientNumber 
	 * @throws SQLException 
	 */
	@SuppressWarnings({ "deprecation", "unchecked" })
	public ComplaintUI(int cn) throws SQLException {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 380, 390);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblDescription = new JLabel("Describe your problem");
		lblDescription.setBounds(5, 60, 145, 14);
		contentPane.add(lblDescription);
		
		JTextArea textArea = new JTextArea();
		
		
		JScrollPane scroll = new JScrollPane(textArea);
		scroll.setBounds(160, 60, 150, 50);
		contentPane.add(scroll);
		
		Date today = new Date(System.currentTimeMillis());
		int dd = today.getDate();
		int mm = today.getMonth()+1; //January is 0!
		int yyyy = today.getYear()+1900; // time is only mesured form 1900 to curent date so we need to add 1900 to ghet the correct year
		
		
		
		String date=""+dd+"/"+mm+"/"+yyyy;
		
		Label lblDate = new Label("Date");
		lblDate.setBounds(10, 10, 107, 22);
		lblDate.setText(date);
		contentPane.add(lblDate);
		
		JComboBox sq = new JComboBox();
		sq.setModel(new DefaultComboBoxModel(new String[] {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"}));
		sq.setBounds(180, 130, 38, 22);
		contentPane.add(sq);
		
		JLabel lblsq = new JLabel("Service quality:");
		lblsq.setHorizontalAlignment(SwingConstants.LEFT);
		lblsq.setBounds(5, 130, 120, 14);
		contentPane.add(lblsq);
		
		JComboBox qr = new JComboBox();
		qr.setModel(new DefaultComboBoxModel(new String[] {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"}));
		qr.setBounds(180, 170, 38, 22);
		contentPane.add(qr);
		
		JLabel lblqr = new JLabel("Quick response:");
		lblqr.setHorizontalAlignment(SwingConstants.LEFT);
		lblqr.setBounds(5, 170, 120, 14);
		contentPane.add(lblqr);
		
		
		JComboBox csq = new JComboBox();
		csq.setModel(new DefaultComboBoxModel(new String[] {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"}));
		csq.setBounds(180, 210, 38, 22);
		contentPane.add(csq);
		
		JLabel lblCostumerServiceQuality = new JLabel("Costumer service quality:");
		lblCostumerServiceQuality.setBounds(5, 210, 150, 14);
		contentPane.add(lblCostumerServiceQuality);
		
		
		JLabel lblSelectComplaint = new JLabel("Select complaint:");
		lblSelectComplaint.setBounds(5, 250, 120, 14);
		contentPane.add(lblSelectComplaint);
		
		
		
		JComboBox idC = new JComboBox();
		for(Complaint c :bll.ComplaintBLL.getAvaiableComplaints()){
			if(c.getClientNumber()==cn){
				
				idC.addItem(c.getIdComplaint());
			}
			
		}
		
		idC.setBounds(180, 250, 38, 20);
		contentPane.add(idC);
		
		JButton btnSend = new JButton("Send");
		btnSend.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				String sqS=(String)sq.getSelectedItem();
				int sqV = Integer.parseInt(sqS) ;
				String qrS=(String)qr.getSelectedItem();
				int qrV = Integer.parseInt(qrS) ;
				String csqS=(String)csq.getSelectedItem();
				int csqV = Integer.parseInt(csqS) ;
			//	String idCS=(String)idC.getSelectedItem();
				int idCV = (int)idC.getSelectedItem();
				Complaint c =new Complaint(textArea.getText(),today,sqV,qrV,csqV,cn,idCV);
				try {
					
	
					System.out.println(c.getIdComplaint());
					bll.ComplaintBLL.updateComplaint(c);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		btnSend.setBounds(120, 320, 90, 25);
		contentPane.add(btnSend);
		
		
	}
	

	
}
