package dao;
import model.*;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;


import connection.*;


public class ClientDAO {
	
	
	public static Client findByUsername(String username) throws SQLException{
		Client toReturn=null;
		for(Client c:getClients()){
			
			String user=c.getUserame();
			
			if(user.equals(username)){
				toReturn=new Client(c.getClientNumber(),c.getUserame(),c.getPassword(),c.getName(),c.getAddress(),c.getIdCardNumber());
			
			
		}
		}
		
		return toReturn;
	}
	
	
	public static Client findById(int  clientNumber) throws SQLException{
		Client toReturn=null;
		for(Client c:getClients()){
			
			int user=c.getClientNumber();
			
			if(user==clientNumber){
				toReturn=new Client(c.getClientNumber(),c.getUserame(),c.getPassword(),c.getName(),c.getAddress(),c.getIdCardNumber());
			
			
		}
		}
		
		return toReturn;
	}
	
	public static void addClient(Client c) throws SQLException {
		Connection con = ConnectionFactory.getConnection();
		Statement s=con.createStatement();
		

	String client="INSERT INTO `mydb`.`client` (`idClient`, `userName`, `password`, `name`, `address`, `idCardNumber`) VALUES (' "
			+c.getClientNumber() + "', '" 
			+ c.getUserame() + "','" 
			+ c.getPassword()+ "','"
			+ c.getName()+"','"
			+c.getAddress()+"','"
			+c.getIdCardNumber() +"');";
			
	

			s.executeUpdate(client);
			
			ConnectionFactory.close(con);
			ConnectionFactory.close(s);
			
	
	}
	
	
	public static void deleteClient(int clinetNumber) throws SQLException{ // functie care sterge un client in baza de date 
		Connection con = ConnectionFactory.getConnection();
		Statement s= con.createStatement();
		String client="DELETE FROM `mydb`.`client` WHERE `idClient`='" +clinetNumber +"';";
				
				
		s.executeUpdate(client);	

		ConnectionFactory.close(con);
		ConnectionFactory.close(s);
	}
	
	
	
	
	public static void updateClient(Client c,int clientNumber)throws SQLException{ // functie care face update la un client in baza de date 
		Connection con = ConnectionFactory.getConnection();
		Statement s= con.createStatement();
		String client="UPDATE `mydb`.`client` SET `idClient`='"+c.getClientNumber()+"', `userName`='"+c.getUserame()
		        +"', `password`='"+c.getPassword()+"', `name`='"+c.getName()+"', `address`='"+c.getAddress()
		        +"', `idCardNumber`='"+c.getIdCardNumber()+"' WHERE `idClient`='"+clientNumber+"';";

		s.executeUpdate(client);	

		ConnectionFactory.close(con);
		ConnectionFactory.close(s);
	}

	

	public static ArrayList<Client> getClients() throws SQLException {

		ArrayList<Client> clients=new ArrayList<Client>();
		Connection con = ConnectionFactory.getConnection();
		Statement myStm2=con.createStatement();
		ResultSet rs2=myStm2.executeQuery("select * from client");
		while(rs2.next()){
			Client c=new Client(rs2.getInt("idClient"),rs2.getString("userName"),rs2.getString("password"),rs2.getString("name"),rs2.getString("address"),rs2.getInt("idCardNumber"));
			clients.add(c);
		//	System.out.println(c.getClientNumber()+" " +c.getUserame()+" "+c.getPassword()+" "+c.getName()+" "+c.getAddress()+" "+c.getIdCardNumber());		
		}

		ConnectionFactory.close(con);
		ConnectionFactory.close(myStm2);
		ConnectionFactory.close(rs2);
		
		 
		return clients;
	}

}
