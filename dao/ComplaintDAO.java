package dao;
import model.*;

import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import connection.*;


public class ComplaintDAO {
	
	public static void addComplaint(Complaint c) throws SQLException {
		Connection con = ConnectionFactory.getConnection();
		Statement s=con.createStatement();
		

	String complaint="INSERT INTO `mydb`.`complaint` (`idComplaint`,`description`, `date`, `serviceQuality`, `quickResponse`, `costrumerServiceQuality`, `Client_idClient`) VALUES ('"
			+c.getIdComplaint()+"','"+c.getDescription()+"', '"+c.getDate()+"', '"+c.getServiceQuality()+"', '"+c.getQuickResponse()+"', '"+c.getCostumerServiceQuality()+"', '"+c.getClientNumber()+"');";

			
			s.executeUpdate(complaint);
			ConnectionFactory.close(con);
			ConnectionFactory.close(s);

	}
	
	
	
	
	public static void updateComplaint(Complaint c) throws SQLException {
		Connection con = ConnectionFactory.getConnection();
		Statement s=con.createStatement();
		

	String complaint="UPDATE `mydb`.`complaint` SET `description`='"+c.getDescription()+"', `date`='"+c.getDate()
			+"', `serviceQuality`='"+c.getServiceQuality()+"', `quickResponse`='"+c.getQuickResponse()
			+"', `costrumerServiceQuality`='"+c.getCostumerServiceQuality()+"' WHERE `idComplaint`='"+c.getIdComplaint()+"';";
 System.out.println(c.getIdComplaint());
		
			s.executeUpdate(complaint);
			ConnectionFactory.close(con);
			ConnectionFactory.close(s);
			
	
	}
	
	
	public static void deleteComplaint(int idComplaint) throws SQLException{ 
		Connection con = ConnectionFactory.getConnection();
		Statement s= con.createStatement();
		String complaint="DELETE FROM `mydb`.`complaint` WHERE `idComplaint`='" +idComplaint +"';";
				
				
		s.executeUpdate(complaint);	

		ConnectionFactory.close(con);
		ConnectionFactory.close(s);
	}
	
	
	public static ArrayList<Complaint> getAvaiableComplaints() throws SQLException {
		ArrayList<Complaint> complaints = new ArrayList<>();
		Connection con = ConnectionFactory.getConnection();
		Statement myStm2=con.createStatement();
		ResultSet rs2=myStm2.executeQuery("select * from complaint where description is NULL");// only show uncopleted complaints
		while(rs2.next()){
       
			Complaint c= new Complaint(rs2.getString("description"),rs2.getDate("date"),rs2.getInt("serviceQuality"),
					rs2.getInt("quickResponse"),
					rs2.getInt("costrumerServiceQuality"),
					rs2.getInt("Client_idClient"),
					rs2.getInt("idComplaint"));
			complaints.add(c);
		}

		ConnectionFactory.close(con);
		ConnectionFactory.close(myStm2);
		ConnectionFactory.close(rs2);
		
		 
		return complaints;
	}
	
	
	public static ArrayList<Complaint> getComplaints() throws SQLException {
		ArrayList<Complaint> complaints = new ArrayList<>();
		Connection con = ConnectionFactory.getConnection();
		Statement myStm2=con.createStatement();
		ResultSet rs2=myStm2.executeQuery("select * from complaint where description is NOT NULL"); // only show copleted complaints
		while(rs2.next()){
       
			System.out.println(rs2.getDate(3));
			
			
			Date date=rs2.getDate(3);	
			Date d=new Date(date.getTime());
		
			
			Complaint c= new Complaint(rs2.getString("description"),d,rs2.getInt("serviceQuality"),
					rs2.getInt("quickResponse"),
					rs2.getInt("costrumerServiceQuality"),
					rs2.getInt("Client_idClient"),
					rs2.getInt("idComplaint"));
			
			c.setDate(d);
			complaints.add(c);
			
			
			
			
		}
		
		

		ConnectionFactory.close(con);
		ConnectionFactory.close(myStm2);
		ConnectionFactory.close(rs2);
		
		 
		return complaints;
	}
	
	

}
