package dao;
import model.*;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import connection.*;


public class EmployeeDAO {
	
	public static  Employee findByUsername(String username) throws SQLException{
		 Employee toReturn=null;
		for( Employee e:getEmployees()){
			
			String user=e.getUserame();
			
			if(user.equals(username)){
				toReturn=new  Employee(e.getIdCardNumber(),e.getUserame(),e.getPassword(),e.getName(),e.getAddress());
			
		}
		}
		
		return toReturn;
	}
	
	
	
	public static void addEmployee( Employee e) throws SQLException {
		Connection con = ConnectionFactory.getConnection();
		Statement s=con.createStatement();
		

	String Employee="INSERT INTO `mydb`.`employee` (`idCardNr`, `userName`, `password`, `name`, `address`) VALUES ('" 
			+e.getIdCardNumber()+ "', '"+e.getUserame()+"', '"+e.getPassword()+"', '"+e.getName()+"', '"+e.getAddress() +"');";


			s.executeUpdate(Employee);
			
			ConnectionFactory.close(con);
			ConnectionFactory.close(s);
			
	
	}
	
	
	public static void deleteEmployee(String user) throws SQLException{ // functie care sterge un Employee in baza de date 
		Connection con = ConnectionFactory.getConnection();
		Statement s= con.createStatement();
		String Employee="DELETE FROM `mydb`.`employee` WHERE `userName`='" +user +"';";
				
				
		s.executeUpdate(Employee);	

		ConnectionFactory.close(con);
		ConnectionFactory.close(s);
	}
	
	
	
	
	public static void updateEmployee(Employee e,String user)throws SQLException{ // functie care face update la un Employee in baza de date 
		Connection con = ConnectionFactory.getConnection();
		Statement s= con.createStatement();
		String Employee="UPDATE `mydb`.`employee` SET `userName`='"+e.getUserame()+"', `password`='"+e.getPassword()
	           	+"', `name`='"+e.getName()+"', `address`='"+e.getAddress()
		        +"', `idCardNumber`='"+e.getIdCardNumber()+"' WHERE `userName`='"+user+"';";

		s.executeUpdate(Employee);	

		ConnectionFactory.close(con);
		ConnectionFactory.close(s);
	}

	

	private static ArrayList<Employee> getEmployees() throws SQLException {

		ArrayList<Employee> Employees=new ArrayList();
		Connection con = ConnectionFactory.getConnection();
		Statement myStm2=con.createStatement();
		ResultSet rs2=myStm2.executeQuery("select * from employee");
		while(rs2.next()){
			Employee e=new Employee(Integer.parseInt(rs2.getString("idCardNr")),rs2.getString("userName"),rs2.getString("password"),rs2.getString("name"),rs2.getString("address"));
			Employees.add(e);
			System.out.println(e.getIdCardNumber()+" " +e.getUserame()+" "+e.getPassword()+" "+e.getName()+" "+e.getAddress()+" "+e.getIdCardNumber());		
		}

		ConnectionFactory.close(con);
		ConnectionFactory.close(myStm2);
		ConnectionFactory.close(rs2);
		
		 
		return Employees;
	}

}
