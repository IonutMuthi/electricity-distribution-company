package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import connection.ConnectionFactory;
import model.Bill;
import model.Request;

public class RequestDAO {
	public static void addRequest(String description, int clientNumber) throws SQLException{
		Connection con = ConnectionFactory.getConnection();
		Statement s=con.createStatement();
		ResultSet rs2=s.executeQuery("select * from complaint");
		rs2.last();
	//String compliant ="SELECT idComplaint FROM mydb.client ORDER BY idComplaint DESC LIMIT 1";
		
	int idC=rs2.getInt("idComplaint")+1;
	                  
	String complaint="INSERT INTO `mydb`.`complaint` (`idComplaint`, `Client_idClient`) VALUES ('"
		            	+idC+"', '"+clientNumber+"');";
	s.execute(complaint);
	String request="INSERT INTO `mydb`.`request` ( `description`, `accepted`, `Client_idClient`,`complaint_idComplaint`) VALUES ('"
			+description+"', '"+0+"', '"+clientNumber+"','"+idC+"');";
	
			s.executeUpdate(request);		
			ConnectionFactory.close(con);
			ConnectionFactory.close(s);
			ConnectionFactory.close(rs2);
	}
	
	
	public static void updateRequest(int id) throws SQLException{
		Connection con = ConnectionFactory.getConnection();
		Statement s=con.createStatement();
	
	 String request=	"UPDATE `mydb`.`request` SET `accepted`='1' WHERE `idRequest`='"+id+"';";
			
	

			s.executeUpdate(request);		
			ConnectionFactory.close(con);
			ConnectionFactory.close(s);
			
	}
	
	


	
	public static ArrayList<Request> getRequests() throws SQLException {
		ArrayList<Request> requests = new ArrayList<>();
		Connection con = ConnectionFactory.getConnection();
		Statement myStm2=con.createStatement();
		ResultSet rs2=myStm2.executeQuery("select * from request");
		while(rs2.next()){

			Request r=new Request(rs2.getInt("idRequest"),rs2.getString("description"),rs2.getBoolean("accepted"),rs2.getInt("Client_idClient"),rs2.getInt("complaint_idComplaint"));
			requests.add(r);
		}

		ConnectionFactory.close(con);
		ConnectionFactory.close(myStm2);
		ConnectionFactory.close(rs2);
		
		 
		return requests;
	}
}
