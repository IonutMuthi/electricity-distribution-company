package dao;
import model.*;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import connection.*;


public class ManagerDAO {
	
	public static  Manager findByUsername(String username) throws SQLException{
		 Manager toReturn=null;
		for( Manager m:getManagers()){
			
			String user=m.getUserame();
			
			if(user.equals(username)){
				toReturn=new  Manager(m.getIdCardNumber(),m.getUserame(),m.getPassword(),m.getName(),m.getAddress());
			
			System.out.println("in if");
		}
		}
		
		return toReturn;
	}
	
	
	
	public static void addManager( Manager m) throws SQLException {
		Connection con = ConnectionFactory.getConnection();
		Statement s=con.createStatement();
		

	String Manager="INSERT INTO `mydb`.`manager` (`idCardNr`, `userName`, `password`, `name`, `address`) VALUES ('"+
	              m.getIdCardNumber()+"', '"+m.getUserame()+"', '"+m.getPassword()+"', '"+m.getName()+"', '"+m.getAddress()+"');";


			s.executeUpdate(Manager);
			
			ConnectionFactory.close(con);
			ConnectionFactory.close(s);
			
	
	}
	
	
	public static void deleteManager(String user) throws SQLException{ // functie care sterge un Manager in baza de date 
		Connection con = ConnectionFactory.getConnection();
		Statement s= con.createStatement();
		String Manager="DELETE FROM `mydb`.`manager` WHERE `userName`='" +user +"';";
				
				
		s.executeUpdate(Manager);	

		ConnectionFactory.close(con);
		ConnectionFactory.close(s);
	}
	
	
	
	
	public static void updateManager(Manager e,String user)throws SQLException{ // functie care face update la un Manager in baza de date 
		Connection con = ConnectionFactory.getConnection();
		Statement s= con.createStatement();
		String Manager="UPDATE `mydb`.`manager` SET `userName`='"+e.getUserame()+"', `password`='"+e.getPassword()
	           	+"', `name`='"+e.getName()+"', `address`='"+e.getAddress()
		        +"', `idCardNumber`='"+e.getIdCardNumber()+"' WHERE `userName`='"+user+"';";

		s.executeUpdate(Manager);	

		ConnectionFactory.close(con);
		ConnectionFactory.close(s);
	}

	

	private static ArrayList<Manager> getManagers() throws SQLException {

		ArrayList<Manager> Managers=new ArrayList();
		Connection con = ConnectionFactory.getConnection();
		Statement myStm2=con.createStatement();
		ResultSet rs2=myStm2.executeQuery("select * from manager");
		while(rs2.next()){
			Manager m=new Manager(Integer.parseInt(rs2.getString("idCardNr")),rs2.getString("userName"),rs2.getString("password"),rs2.getString("name"),rs2.getString("address"));
			Managers.add(m);
			System.out.println(m.getIdCardNumber()+" " +m.getUserame()+" "+m.getPassword()+" "+m.getName()+" "+m.getAddress()+" "+m.getIdCardNumber());		
		}

		ConnectionFactory.close(con);
		ConnectionFactory.close(myStm2);
		ConnectionFactory.close(rs2);
		
		 
		return Managers;
	}

}
