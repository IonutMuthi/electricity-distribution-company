package dao;
import model.*;

import java.sql.Connection;
//import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;


import connection.*;


public class BillDAO {
	
	public static Bill findById(int  id, int cId) throws SQLException{
		Bill toReturn=null;
		for(Bill b:getBills(cId)){
			
	
			if(b.getId()==id){
				toReturn=new Bill(b.getId(),b.getPrice(),b.getEmited(),b.getScadent(),b.isPaid());
			
			
		}
		}
		
		return toReturn;
	}
	
	
	
	
	
	public static void addBill(Bill b,int idC) throws SQLException {
		Connection con = ConnectionFactory.getConnection();
		Statement s=con.createStatement();
		

	String client="INSERT INTO `mydb`.`bill` (`idBill`, `price`, `emited`, `scadent`, `paid`, `Client_idClient`) VALUES ('"
			+ b.getId()+"', '"+b.getPrice()+"', '"+b.getEmited()+"', '"+b.getScadent()+"', '"+b.isPaid()+"', '"+idC+"');";

			s.executeUpdate(client);
			
			ConnectionFactory.close(con);
			ConnectionFactory.close(s);
			
	
	}
	
	
	public static void deleteBill(int idBill) throws SQLException{ 
		Connection con = ConnectionFactory.getConnection();
		Statement s= con.createStatement();
		String bill="DELETE FROM `mydb`.`bill` WHERE `idBill`='" +idBill +"';";
				
				
		s.executeUpdate(bill);	

		ConnectionFactory.close(con);
		ConnectionFactory.close(s);
	}
	
	
	
	
	public static void updateBill(Bill b,int idC)throws SQLException{ 
		Connection con = ConnectionFactory.getConnection();
		Statement s= con.createStatement();
		int paid=0;
		if(b.isPaid()){
			paid=1;
		}
		String bill="UPDATE `mydb`.`bill` SET `price`='"+b.getPrice()+"', `emited`='"+b.getEmited()+
				"', `scadent`='"+b.getScadent()+"', `paid`='"+paid+
				"', `Client_idClient`='"+idC+"' WHERE `idBill`='"+b.getId()+"';";

		s.executeUpdate(bill);	

		ConnectionFactory.close(con);
		ConnectionFactory.close(s);
	}

	

	public static ArrayList<Bill> getBills(int clientNumber ) throws SQLException {
		ArrayList<Bill> bills = new ArrayList<Bill>();
		Connection con = ConnectionFactory.getConnection();
		Statement myStm2=con.createStatement();
		ResultSet rs2=myStm2.executeQuery("select * from bill");
		while(rs2.next()){
			
			Bill b=new Bill(rs2.getInt("idBill"),rs2.getDouble("price"),rs2.getDate("emited"),rs2.getDate("scadent"),rs2.getBoolean("paid"));
			if((rs2.getInt("Client_idClient"))==clientNumber){
			bills.add(b);
			}
			
		}

		ConnectionFactory.close(con);
		ConnectionFactory.close(myStm2);
		ConnectionFactory.close(rs2);
		
		 
		return bills;
	}
	
	

}
