package bll;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.NoSuchElementException;

import bll.validators.ClientUsernameValidator;
import bll.validators.ManagerUsernameValidator;
import bll.validators.Validator;
import dao.ClientDAO;
import dao.ManagerDAO;
import model.Client;
import model.Manager;

public class ManagerBLL {


	private ArrayList<Validator<Manager>> validators;

	public ManagerBLL() {
		validators = new ArrayList<Validator<Manager>>();
		validators.add(new ManagerUsernameValidator());
		

	}

	
	public static Manager findManagerByUsername(String user) throws SQLException{
		
		
		return ManagerDAO.findByUsername(user);
    	 
	
		
	}
	
	public void insertClient(Manager m) throws SQLException {
		for (Validator<Manager> v : validators) {
			v.validate(m);
		}
		ManagerDAO.addManager(m);;
	}
	
	public void deleteManager(String user)throws SQLException{
		ManagerDAO.deleteManager(user);
	}
	
	public void updateManager(Manager e,String user) throws SQLException{
		ManagerDAO.updateManager(e, user);
	}

	
	
	
}
