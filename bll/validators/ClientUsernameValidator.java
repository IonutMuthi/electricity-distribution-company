package bll.validators;

import java.sql.SQLException;

import dao.ClientDAO;
import model.Client;

public class ClientUsernameValidator implements Validator<Client> {

	ClientDAO daoC=new ClientDAO();
	
	@Override
	public void validate(Client c) {
		Client c1;
		try {
			c1 = daoC.findByUsername(c.getUserame());
			if(c1!=null){
				throw new IllegalArgumentException("A client with that username already exists");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}

}
