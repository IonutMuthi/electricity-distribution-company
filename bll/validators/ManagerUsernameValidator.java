package bll.validators;

import java.sql.SQLException;

import dao.ManagerDAO;
import model.Manager;

public class ManagerUsernameValidator  implements Validator<Manager> {

	ManagerDAO daoM=new ManagerDAO();
	
	@Override
	public void validate(Manager m) {
		Manager m1;
		try {
			m1 = daoM.findByUsername(m.getUserame());
			if(m1!=null){
				throw new IllegalArgumentException("A manager with that username already exists");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}

	
}
