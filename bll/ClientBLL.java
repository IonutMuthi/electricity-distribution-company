package bll;

import java.sql.SQLException;
import java.util.ArrayList;

import bll.validators.ClientUsernameValidator;
import bll.validators.Validator;
import dao.ClientDAO;
import model.Client;



public class ClientBLL {
	

	private static ArrayList<Validator<Client>> validators;

	public ClientBLL() {
		validators = new ArrayList<Validator<Client>>();
		validators.add(new ClientUsernameValidator());
		

	}

	
	
	
	public static void insertClient(Client c) throws SQLException {
//		for (Validator<Client> v : validators) {
//			v.validate(c);
//		}
		ClientDAO.addClient(c);
	}
	
	public static void deleteClient(int id) throws SQLException{
		ClientDAO.deleteClient(id);
	}
	
	public static void updateClient(Client c, int id) throws SQLException{
		ClientDAO.updateClient(c, id);
	}
	
	public static ArrayList<Client> getClients() throws SQLException{
		return ClientDAO.getClients();
	}


	public static Client findByUsername(String user) throws SQLException {

		return ClientDAO.findByUsername(user);
	}




	public static Client findById(int cnr) throws SQLException {
		// TODO Auto-generated method stub
		return ClientDAO.findById(cnr);
	}

	
	
	
	
	
	

}
