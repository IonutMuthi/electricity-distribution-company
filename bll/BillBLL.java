package bll;

import java.sql.SQLException;
import java.util.ArrayList;

import dao.BillDAO;
import model.Bill;

public class BillBLL {
	
	

	public static void addBill(Bill c,int idc) throws SQLException {
		
		BillDAO.addBill(c,idc);
	}
	
	public static void deleteBill(int id) throws SQLException{
		BillDAO.deleteBill(id);
	}
	
	public static void updateBill(Bill c,int idC) throws SQLException{
		BillDAO.updateBill(c, idC);
	}
	
	public static ArrayList<Bill> getBills(int cn) throws SQLException{
		return BillDAO.getBills(cn);
	}

	
	

}
