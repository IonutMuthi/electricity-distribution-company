package bll;

import java.sql.SQLException;
import java.util.ArrayList;

import bll.validators.Validator;
import dao.ComplaintDAO;
import model.Complaint;
import model.Complaint;

public class ComplaintBLL {
	

	
	public static void addComplaint(Complaint c) throws SQLException {
		
		ComplaintDAO.addComplaint(c);
	}
	
	public static void deleteComplaint(int id) throws SQLException{
		ComplaintDAO.deleteComplaint(id);
	}
	
	public static void updateComplaint(Complaint c) throws SQLException{
		ComplaintDAO.updateComplaint(c);
	}
	
	public static ArrayList<Complaint> getComplaints() throws SQLException{
		return ComplaintDAO.getComplaints();
	}

	
	
	public static ArrayList<Complaint> getAvaiableComplaints() throws SQLException {
	      return ComplaintDAO.getAvaiableComplaints();	
	}
	
	

}
